//dependencies
const User = require("../models/user.js");
const Product = require("../models/product.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js")

// Register
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin

	})

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}

// Login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); // true or false

			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return {access: auth.createAccessToken(result)};

			}
			else{
				// If password does not match, else
				return false;
			}
		}
	})
}

// order feature
module.exports.order = async (request, response) => {
		const userData = auth.decode(request.headers.authorization);

		let productName = await Product.findById(request.body.productId).then(result => result.name);

		let newData = {
			userId: userData.id,
			email: userData.email,
			productId: request.body.productId,
			//try
			totalAmount: request.body.totalAmount,
			quantity: request.body.quantity,
			productName: productName
		}
		console.log("thisIsTheNewDataObject");
		console.log(newData);

		let isUserUpdated = await User.findById(newData.userId).then (user => {
			user.orders.push({
				totalAmount: newData.totalAmount,
				products: [
					{
						productId: newData.productId,
						quantity: newData.quantity
					}
				]
			})
			
			/*	//try
			user.orders.products.push({
				quantity: newData.quantity
			})
			*/

			return user.save()
			.then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			})
		})
		console.log(isUserUpdated);
/*
		let isProductUpdated = await Product.findById(newData.productId).then(product => {
			product.orders.push({
				userId: newData.userId,
				email: newData.email,
				quantity: newData.quantity

			})

			product.stocks -= 1;

				return product.save()
				.then(result => {
					console.log(result);
					return true;
				})
				.catch(error => {
					console.log(error);
					return false;
				})
		})

		console.log(isProductUpdated);
*/


/*		(isUserUpdated == true && isProductUpdated == true)?
			response.send(true) : response.send(false);*/

		if(isUserUpdated == true){
			response.send(true)  
		}


}


// Get user details
module.exports.getUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

		return User.findById(userData.id).then(result => {
			result.password = "*****";
			response.send(result);
		})


}
