const mongoose = require("mongoose");
const Product = require("../models/product.js");


// Create Product
module.exports.addProduct = (reqBody, addedProduct) =>{
	if(addedProduct.isAdmin == true){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			stocks: reqBody.stocks
		})
	return newProduct.save().then((newProduct, error)=>{
		if(error){
			return error;
		}
		else{
			return newProduct;
		}
	})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}


}

// GET all active Products
module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result =>{
		return result;
	})
}


// Retrieve Single Product
module.exports.getSingleProduct = (productId) =>{
	return Product.findById(productId).then(result =>{
		return result;
	})
}


// Update Product Information
module.exports.updateProduct = (productId, newData) =>{
			if(newData.isAdmin == true){
				return Product.findByIdAndUpdate(productId,
					{
						name: newData.product.name,
						description: newData.product.description,
						price: newData.product.price,
						stocks: newData.product.stocks	
					}
				).then((result, error)=>{
					if(error){
						return false;
					}
					else{
						return true;
					}
				})
			}

			else{
				let message = Promise.resolve('User must be ADMIN to access this functionality');
				return message.then((value) => {return value});

			}
}

// Archive Product
module.exports.archiveProduct = (productId, archiveData) =>{
			if(archiveData.isAdmin == true){
				return Product.findByIdAndUpdate(productId,
				{
					isActive: archiveData.product.isActive
				}
			).then((result, error) => {
				if(error){
					return false;
				}
				else{
					return true
				}
			})
		}
		else{
			let message = Promise.resolve('User must be ADMIN to acces this functionality');
			return message.then((value) => {return value});
		}
}