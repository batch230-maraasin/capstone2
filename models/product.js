// dependency
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
			{
				name: {
					type: String,
					required: [true, "Product is required"]
				},

				description: {
					type: String,
					required: [true, "Description is required"]
				},

				price: {
					type: Number,
					required: [true, "Price is required"]
				},

				stocks:{
					type: Number,
					required: [true, "Stocks is required"]
				},

				isActive:{
					type: Boolean,
					default: true
				},

				createdOn:{
					type: Date,
					// The "new Date" expression instatiates a new "Date" that the current date and time whenever a course is created in our database
					default: new Date()
				},

				orders: [

					{
						orderId:{
							type: String,
							required: [true, "orderId is required"]
						},

						userId:{
							type: String,
							required: [true, "userId is required"]
						},

						userEmail:{
							type: String,
							required: [true, "userEmail is required"]
						},

						quantity:{
							type: Number,
							required: [true, "quantity is required"]
						},

						purchasedOn: {
							type: Date,
							default: new Date()
						}
					}
				]

			}
		)

module.exports = mongoose.model("Product", productSchema);