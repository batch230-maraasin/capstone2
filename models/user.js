// dependency
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
			{
				firstName: {
					type: String,
					required: [true, "First name is required"]
				},

				lastName: {
					type: String,
					required: [true, "Last name is required"]
				},

				email: {
					type: String,
					required: [true, "Email is required"]
				},

				password: {
					type: String,
					required: [true, "Password is required"]
				},

				mobileNo: {
					type: String,
					required: [true, "Mobile number is required"]
				},

				isAdmin:{
					type: Boolean,
					default: false
				},

				orders: [
					
					{
						totalAmount: { 
							type: Number,
							required: [true, "totalAmount is required"]

						},

						purchasedOn: {
							type: Date,
							default: new Date()
						}, 

						products: [

							{
								productId:{
									type: String,
									required: [true, "productId is required"]

								},

								//productName - optional

								quantity:{
									type: Number,
								
									required: [true, "quantity is required"]
								}
							}

						]
					}

				]

			}
		)


module.exports = mongoose.model("User", userSchema);