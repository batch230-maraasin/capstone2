const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/register", (request, response) => {
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController))
})

router.post("/login", (request, response)=>{
	userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})

/*
router.post("/checkout", auth.verify, (request, response)=>{
	userControllers.order(request.body).then(resultFromController => response.send(
		resultFromController))
})
*/

router.post("/checkout", auth.verify, userControllers.order)

router.get("/details", auth.verify, userControllers.getUserDetails);


module.exports = router;