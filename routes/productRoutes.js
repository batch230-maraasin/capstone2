const express = require ("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");


// Create Product
router.post("/create", auth.verify, (request, response) =>{
		const addedProduct = {
			product: request.body,

			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}
		productControllers.addProduct(request.body, addedProduct).then(resultFromController => response.send(resultFromController)
		)
})

//GET all ACTIVE Products
router.get("/active", (request, response) => {
		productControllers.getActiveProducts().then(resultFromController => response.send(resultFromController))
});


// Retrieve Single Product
router.get("/:productId", (request, response) =>{
	productControllers.getSingleProduct(request.params.productId).then(resultFromController => response.send(resultFromController));
})

// Update Product Information
router.patch("/:productId/update", auth.verify, (request, response) => 
{
		const newData = {
			product: request.body, //request.headers.authorization contains jwt
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		productControllers.updateProduct(request.params.productId,newData).then(resultFromController =>{
			response.send(resultFromController)
		})
})

// Archive Product
router.patch("/:productId/archive", auth.verify, (request, 
	response) => 
{
	const productId = request.params.productId;
	const archiveData = {
		product: request.body, //request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin

	}

	productControllers.archiveProduct(request.params.courseId, archiveData).then(resultFromController => {
		response.send(resultFromController)
	})
})












module.exports = router;